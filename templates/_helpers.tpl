{{/*
Expand the name of the chart.
*/}}
{{- define "tenant-app.name" -}}
{{- .Values.name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "tenant-app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "tenant-app.labels" -}}
helm.sh/chart: {{ include "tenant-app.chart" . }}
{{ include "tenant-app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "tenant-app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "tenant-app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{ toYaml .Values.commonLabels }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "tenant-app.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "tenant-app.name" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "tenant-app.domainName" -}}
{{ . | replace "." "-" }}
{{- end }}

{{- define "tenant-app.certSecretName" -}}
tls-{{ include "tenant-app.domainName" . }}
{{- end }}